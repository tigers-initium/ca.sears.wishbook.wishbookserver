# API Server for Wish Book app

## Docker

### To start the server with Docker

1. Install [Docker](https://www.docker.com/products/overview)
2. Make sure you can use the `docker-compose` command (```docker-compose version```)
3. Clone this repo
4. Change to cloned directory
5. Run ```docker-compose up```

    The Node app and CouchDB are accessable at [http://localhost:8080](http://localhost:8080) and [http://localhost:5984](http://localhost:5984) respectively

### To stop the containers

1. Press `Ctrl+c`
2. Wait for containers to stop

    If containers don't stop quickly enough, press `Ctrl+c` again to force stop

## Vagrant

### To start the server with Vagrant

1. Install [Vagrant](https://www.vagrantup.com/downloads.html) for your system
2. Clone this repo
3. Change to cloned directory
4. Run ```vagrat up```

    The server should output its ip address (default is `192.168.33.11`) and the port the where the API is accessable.

#### To get in the Vagrant Box

1. Press `Ctrl+c` twice
2. Type `vagrant ssh` and hit enter
3. The project folder is located at `/vagrant`

### To stop the server

1. Press `Ctrl+c` twice
2. Then type `vagrant halt` and hit enter


## MISC

1. Globally install `eslint` for linting support
    * ```npm install --global eslint```
2. Add `editorconfig` support to your editor if not already present
    * [Atom](https://github.com/sindresorhus/atom-editorconfig)
    * [VS Code](https://github.com/editorconfig/editorconfig-vscode)
    * [Sublime Text](https://github.com/sindresorhus/editorconfig-sublime)
